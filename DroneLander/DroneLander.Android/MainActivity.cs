﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using CafeLib.Core.IoC;
using CafeLib.Mobile.Core.Services;
using DroneLander.Droid.Services;
using DroneLander.Services;

namespace DroneLander.Droid
{
    [Activity(Label = "DroneLander", Icon = "@drawable/icon", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            IServiceRegistry registry = new MobileServices();

            registry.AddSingleton<IAudioService, AudioService>();

            base.OnCreate(savedInstanceState);
            Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App(registry));
        }
    }
}