﻿using System;
using System.Diagnostics;
using CafeLib.Core.IoC;
using DroneLander.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DroneLander
{
    // ReSharper disable once RedundantExtendsListEntry
    public partial class App : Application
    {
        private readonly IServiceRegistry _registry;

        public App(IServiceRegistry registry)
        {
            _registry = registry;
            InitializeComponent();
        }

        protected override void OnStart()
        {
            try
            {
                Bootstrapper.InitApplication(_registry);

                // Initialize the root page.
                MainPage = _registry.GetResolver().Resolve<MainViewModel>().AsNavigationPage();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
