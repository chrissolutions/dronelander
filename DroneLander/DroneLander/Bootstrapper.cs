﻿using CafeLib.Core.IoC;
using DroneLander.ViewModels;

namespace DroneLander
{
    public static class Bootstrapper
    {
        /// <summary>
        /// Initialize the Navigation Page here.
        /// </summary>
        /// <returns>navigation page</returns>
        public static void InitApplication(IServiceRegistry registry)
        {
            registry.AddSingleton<MainViewModel, MainViewModel>();
        }
    }
}
